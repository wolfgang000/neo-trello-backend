from django.conf.urls import url
from django.conf import settings
from . import views

urlpatterns = [
	url(r'^lists/$', views.ListList.as_view(),name='list.list'),
	url(r'^lists/(?P<pk>\d+)/$', views.ListDetail.as_view(),name='list.detail'),
]