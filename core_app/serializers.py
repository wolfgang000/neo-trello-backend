from rest_framework import serializers
from django.core.urlresolvers import reverse
from . import models

class List(serializers.ModelSerializer):
	href = serializers.SerializerMethodField()

	class Meta:
		model = models.List
		fields = ('id', 'title', 'href')
	def get_href(self, obj):
		request = self.context.get('request', None)
		if request is not None:
			uri = request.build_absolute_uri(reverse('list.detail',args = [obj.id]))
		else:
			uri = reverse('list.detail', args = [obj.id])
		return uri