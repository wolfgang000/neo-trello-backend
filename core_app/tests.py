from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from . import models

class Useful:
	def add_fake_list(self, title='Fake title'):
		new_item = models.List(title = title)
		new_item.save()
		return new_item
	
	def add_fake_lists(self):
		items = []
		for i in range(0,5):
			new_item = self.add_fake_list(title ='Super name'+ str(i))
			items.append(new_item)
		return items


####################
# Unit tests
####################

class ItemList(TestCase,Useful):
	
	def test_add_List(self):
		self.add_fake_list()

	def test_add_Lists(self):
		self.add_fake_lists()


####################
# Integration tests
####################

class ItemViewTests(TestCase,Useful):
	def setUp(self):
		self.client =  APIClient()
	
	def check_list(self, item):
		self.assertIn('title',item.keys()) 
		self.assertIn('id',item.keys()) 
		self.assertIn('href',item.keys()) 
		
		# Test if the href return the same resource
		response = self.client.get(item['href'])
		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_get_lists(self):
		self.add_fake_lists()
		response = self.client.get(reverse('list.list'))
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		first_item = response.data[0]
		self.check_list(first_item)

		
	def test_create_item(self):
		request_item = {'title' : 'New Item'}
		response = self.client.post(reverse('list.list'), request_item)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.check_list(response.data)
	
	def test_delete_item(self):
		item = self.add_fake_lists()[0]
		response = self.client.delete(reverse('list.detail',args = [item.id]))
		self.assertEqual(response.status_code,status.HTTP_200_OK )

		response = self.client.get(reverse('list.detail',args = [item.id]))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND )
