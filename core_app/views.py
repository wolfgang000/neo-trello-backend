from django.shortcuts import render
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.settings import api_settings

from django.urls import reverse
from django.http import Http404
from . import serializers
from . import models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


class ListDetail(APIView):
	
	def get_object(self, pk):
		try:
			return models.List.objects.get(pk=pk)
		except ObjectDoesNotExist:
			raise Http404

	def get(self, request, pk,):
		obj = self.get_object(pk)
		serializer = serializers.List(obj, context={'request':request} )
		return Response(serializer.data)

	def patch(self, request, pk,):
		obj = self.get_object(pk)
		serializer = serializers.List(obj, data=request.data, partial=True, context={'request':request})
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk,):
		obj = self.get_object(pk)
		obj.delete()
		return Response(status=status.HTTP_200_OK)


class ListList(APIView):

	def post(self, request,):
		serializer = serializers.List(data=request.data, context={'request':request})
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def get(self, request,):
		orders = models.List.objects.all()
		serializer = serializers.List(orders, many=True, context={'request':request})
		return Response(serializer.data)
